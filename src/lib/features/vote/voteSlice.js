import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    students: ['김길동', '홍길동', '나길동'],
    questions: [
        { kind: 'GOOD', title: '책상이 가장 깨끗한 학생은?' },
        { kind: 'BED', title: '책상이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '주변이 가장 깨끗한 학생은?' },
        { kind: 'BED', title: '옷이 가장 더러운 학생은?' },
        { kind: 'GOOD', title: '가방이 가장 깨끗한 학생은?' }
    ],
    votes: {},
    currentQuestionIndex: 0,
    selections: {},
};

const voteSlice = createSlice({
    name: 'vote',
    initialState,
    reducers: {
        select: (state, action) => {
            const { student, question } = action.payload;
            if (!state.selections[student]) {
                state.selections[student] = { GOOD: [], BED: [] };
            }
            state.selections[student][question.kind].push(question);
        },
        nextQuestion: (state) => {
            for (const student in state.selections) {
                if (!state.votes[student]) {
                    state.votes[student] = { GOOD: 0, BED: 0 };
                }
                state.votes[student].GOOD += state.selections[student].GOOD.length;
                state.votes[student].BED += state.selections[student].BED.length;
            }
            state.selections = {};
            state.currentQuestionIndex++;
        },
    },
});

export const { select, nextQuestion } = voteSlice.actions;

export default voteSlice.reducer;